#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <set>

using namespace std;

void solve() {
	int n, answ = 0, temp;

	cin >> n;
	vector<int> arr, carr;
	set<int> cset;

	for (int i = 0; i < n; i++) {
		cin >> temp;
		arr.push_back(temp);
	}

	for (auto i : arr) {
		cset.insert(i);
	}
	copy(cset.begin(), cset.end(), back_inserter(carr));
	sort(carr.begin(), carr.end());
	
	bool go_back = true;
	auto iter = cset.begin();
	for (int i = 0; i < arr.size(); i++, iter++) {
		cout << arr[i] << " and " << *iter << endl; 
		if (arr[i] > *iter) {
			answ++;

			if (iter != cset.begin()) {
				iter--;
				go_back = false;
			}

			cset.erase(arr[i]);
		}

		if (iter == cset.end() && go_back) {
			iter--;
		}

		go_back = true;
	}
	
	cout << answ << endl;

}

	int main() {
		int t;
		
		cin >> t;

		while(t--) {
			solve();
		}

		return 0;
	}
